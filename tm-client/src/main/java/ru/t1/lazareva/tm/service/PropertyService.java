package ru.t1.lazareva.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['server.port']}")
    public String serverPort;

    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['application.name']}")
    public String applicationName;

    @NotNull
    @Value("#{environment['application.config']}")
    public String applicationConfig;

    @NotNull
    @Value("#{environment['application.log']}")
    public String applicationLog;

    @NotNull
    @Value("#{environment['email']}")
    public String authorEmail;

    @NotNull
    @Value("#{environment['developer']}")
    public String authorName;

}
