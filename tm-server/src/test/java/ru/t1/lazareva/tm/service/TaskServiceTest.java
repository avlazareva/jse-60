package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.constant.ProjectTestData;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.TaskNotFoundException;
import ru.t1.lazareva.tm.exception.field.DescriptionEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.NameEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private ITaskDtoService service;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        userService.add(USER_1);
        userService.add(USER_2);
        service.add(USER_1.getId(), USER_TASK1);
        service.add(USER_2.getId(), USER_TASK2);
    }

    @After
    public void after() throws Exception {
        service.clear();
        userService.clear();
    }

    @Test
    public void add() throws Exception {
        service.clear();
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_TASK1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.add(USER_TASK1.getUserId(), null);
        });
        service.clear();
        Assert.assertNotNull(service.add(USER_TASK1.getUserId(), USER_TASK1));
        @Nullable final TaskDto task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.add(ProjectTestData.USER_1.getId(), NULL_TASK);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_TASK1);
        });
        service.clear();
        Assert.assertNotNull(service.add(USER_TASK1.getUserId(), USER_TASK1));
        @Nullable final TaskDto task = service.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<TaskDto> tasksFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USER_1.getId(), "");
        });
        Assert.assertFalse(service.existsById(USER_1.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById(NON_EXISTING_TASK_ID);
        });
        @Nullable final TaskDto task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_1.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_TASK1.getId());
        });
        Assert.assertNull(service.findOneById(USER_1.getId(), NON_EXISTING_TASK_ID));
        @Nullable final TaskDto task = service.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void clear() throws Exception {
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear();
        service.add(USER_1.getId(), USER_TASK1);
        service.clear(USER_1.getId());
        Assert.assertEquals(0, service.getSize(USER_1.getId()));
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(null);
        });
        service.clear();
        @Nullable final TaskDto createdTask = service.create(USER_1.getId(), USER_TASK1.getName());
        service.remove(createdTask);
        Assert.assertEquals(0, service.findAll(USER_TASK1.getUserId()).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(USER_1.getId(), null);
        });
        service.clear();
        @NotNull final TaskDto createdTask = service.create(USER_1.getId(), USER_TASK1.getName());
        service.remove(USER_1.getId(), createdTask);
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_1.getId(), "");
        });
        service.removeById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNull(service.findOneById(ProjectTestData.USER_1.getId(), USER_TASK1.getId()));
        @Nullable final TaskDto createdTask = service.add(USER_TASK1);
        service.removeById(USER_1.getId(), createdTask.getId());
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        service.clear();
        service.add(USER_TASK1);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
        Assert.assertEquals(0, service.getSize(USER_1.getId()));
        service.add(USER_TASK1);
        Assert.assertEquals(1, service.getSize(USER_1.getId()));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_TASK1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_TASK1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ProjectTestData.USER_1.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ProjectTestData.USER_1.getId(), "");
        });
        @NotNull final TaskDto task = service.create(USER_1.getId(), USER_TASK1.getName());
        Assert.assertEquals(task.getId(), service.findOneById(USER_1.getId(), task.getId()).getId());
        Assert.assertEquals(USER_TASK1.getName(), task.getName());
        Assert.assertEquals(USER_1.getId(), task.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(USER_1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(USER_1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(USER_1.getId(), USER_TASK1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(USER_1.getId(), USER_TASK1.getName(), "");
        });
        @NotNull final TaskDto task = service.create(USER_1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        Assert.assertEquals(task.getId(), service.findOneById(USER_1.getId(), task.getId()).getId());
        Assert.assertEquals(USER_TASK1.getName(), task.getName());
        Assert.assertEquals(USER_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(USER_1.getId(), task.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_1.getId(), null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_1.getId(), "", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_1.getId(), USER_TASK1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_1.getId(), USER_TASK1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateById(USER_1.getId(), NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        @NotNull final String name = USER_TASK1.getName();
        @NotNull final String description = USER_TASK1.getDescription();
        service.updateById(USER_1.getId(), USER_TASK1.getId(), name, description);
        Assert.assertEquals(name, USER_TASK1.getName());
        Assert.assertEquals(description, USER_TASK1.getDescription());
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.NOT_STARTED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById(null, USER_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById("", USER_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USER_1.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USER_1.getId(), "", status);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusById(USER_1.getId(), NON_EXISTING_TASK_ID, status);
        });
        service.changeTaskStatusById(USER_1.getId(), USER_TASK1.getId(), status);
        Assert.assertNotNull(USER_TASK1);
        Assert.assertEquals(status, USER_TASK1.getStatus());
    }

}

