package ru.t1.lazareva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.repository.model.IRepository;
import ru.t1.lazareva.tm.api.service.model.IService;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected IRepository<M> repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M add(@NotNull final M model) throws Exception {
        repository.add(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null) throw new IdEmptyException();
        return repository.findOneByIndex(index);
    }

    @Override
    public int getSize() throws Exception {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final M model) throws Exception {
        if (model == null) return;
        repository.remove(model);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    @SneakyThrows
    @Transactional
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        resultModel = repository.update(model);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

}

