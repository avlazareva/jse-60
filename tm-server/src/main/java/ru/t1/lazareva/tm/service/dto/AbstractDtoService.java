package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.api.service.dto.IDtoService;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;

import java.util.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractModelDto, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected IDtoRepository<M> repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        resultModel = repository.add(model);
        return resultModel;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels = new ArrayList<>();
        for (@NotNull final M model : models) {
            @NotNull final M resultModel = repository.add(model);
            resultModels.add(resultModel);
        }
        return resultModels;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(@Nullable final Integer index) throws Exception {
        if (index == null) return false;
        return repository.existsByIndex(index);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }


    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        resultModel = repository.findOneById(id);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) throw new IdEmptyException();
        @Nullable M resultModel;
        resultModel = repository.findOneByIndex(index);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize() throws Exception {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final Integer index) {
        @Nullable M result = findOneByIndex(index);
        remove(result);
    }

    @Override
    @SneakyThrows
    @Transactional
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @Nullable M resultModel;
        resultModel = repository.update(model);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;

    }

}
